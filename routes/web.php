<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('admin/dashboard');
});

Route::get('/', function () {
    return view('auth.register');
});

Route::get('/verify', function () {
    return view('auth.verify');
})->name('verify');

Route::get('/home', function () {
    return view('home');
})->name('home');

Route::post('/store', [App\Http\Controllers\AuthController::class, 'create'])->name('register');
Route::post('/verify',  [App\Http\Controllers\AuthController::class, 'verify'])->name('verify');

Auth::routes();

// lock-unlock screen
Route::get('login/locked', [App\Http\Controllers\Auth\LoginController::class,'locked'])->middleware('auth')->name('login.locked');
Route::post('login/locked', [App\Http\Controllers\Auth\LoginController::class,'unlock'])->name('login.unlock');
// end of lock route 

// Route::group(['middleware' => ['auth.lock']], function () {
//     Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// });

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/lang/{lang}', [App\Http\Controllers\LocalizationController::class,'switchLang'])->name('langswitch');
Route::get('auth/google', [App\Http\Controllers\Auth\GoogleController::class,'redirectToGoogle']);
Route::get('auth/google/callback', [App\Http\Controllers\Auth\GoogleController::class, 'handleGoogleCallback']);
Route::get('/google', [App\Http\Controllers\Auth\GoogleController::class, 'index'])->name('map');
Route::post('/google/store', [App\Http\Controllers\Auth\GoogleController::class, 'store'])->name('map.store');
Route::post('/google/search', [App\Http\Controllers\Auth\GoogleController::class, 'search'])->name('map.search');
Route::get('/chart', [App\Http\Controllers\ProductController::class, 'index'])->name('chart');
Route::get('/counter', [App\Http\Controllers\ProductController::class, 'counter'])->name('counter');
Route::get('/calender', [App\Http\Controllers\ProductController::class, 'calender'])->name('calender');
Route::post('/calenderAjax', [App\Http\Controllers\ProductController::class, 'calenderAjax']);
Route::get('/products', [App\Http\Controllers\ProductController::class, 'products'])->name('products');
Route::get('/products/replicate/{id}', [App\Http\Controllers\ProductController::class, 'replicate'])->name('product.replicate'); 


Route::group(['prefix' => 'users'], function(){
    Route::get('/index',[App\Http\Controllers\UserController::class,'index'])->name('index');
    Route::get('/create',[App\Http\Controllers\UserController::class,'create'])->name('users.create');
    Route::post('/store',[App\Http\Controllers\UserController::class,'store'])->name('users.store');
    Route::get('/delete/{id}',[App\Http\Controllers\UserController::class,'destroy'])->name('users.delete');
    Route::get('importExportView', [App\Http\Controllers\UserController::class, 'importExportView'])->name('importExportView');
	Route::get('export', [App\Http\Controllers\UserController::class, 'export'])->name('export');
	Route::post('import',[App\Http\Controllers\UserController::class, 'import'])->name('import');
    // Route::get('user','Controller@post');
});

Route::group(['prefix' => 'exams'], function(){
    Route::get('/index',[App\Http\Controllers\ExamController::class,'index'])->name('exams.index');
    Route::get('/create',[App\Http\Controllers\ExamController::class,'create'])->name('exams.create');
    Route::post('/store',[App\Http\Controllers\ExamController::class,'store'])->name('exams.store');
    Route::get('/edit/{id}',[App\Http\Controllers\ExamController::class,'edit'])->name('exams.edit');
    Route::post('/update/{id}',[App\Http\Controllers\ExamController::class,'update'])->name('exams.update');
    Route::get('/delete/{id}',[App\Http\Controllers\ExamController::class,'destroy'])->name('exams.delete');
    Route::get('/questions/{id}',[App\Http\Controllers\ExamController::class,'question'])->name('exams.question');
});

Route::group(['prefix' => 'questions'], function(){
    Route::get('/index',[App\Http\Controllers\QuestionController::class,'index'])->name('questions.index');
    Route::get('/create/{id}',[App\Http\Controllers\QuestionController::class,'create'])->name('questions.create');
    Route::post('/store',[App\Http\Controllers\QuestionController::class,'store'])->name('questions.store');
    Route::get('/edit/{id}',[App\Http\Controllers\QuestionController::class,'edit'])->name('questions.edit');
    Route::post('/update/{id}',[App\Http\Controllers\QuestionController::class,'update'])->name('questions.update');
    Route::get('/delete/{id}',[App\Http\Controllers\QuestionController::class,'destroy'])->name('questions.delete');

});

Route::group(['prefix' => 'students'], function(){
    Route::get('/exams',[App\Http\Controllers\StudentQuestionAnswerController::class,'index'])->name('students.exams');
    Route::get('/questions/{id}',[App\Http\Controllers\StudentQuestionAnswerController::class,'questions'])->name('students.questions');
    Route::get('/store',[App\Http\Controllers\StudentQuestionAnswerController::class,'store'])->name('students.store');
    Route::get('/result',[App\Http\Controllers\StudentQuestionAnswerController::class,'result'])->name('students.result');

});

Route::group(['prefix' => 'exam_result'], function(){
    Route::get('/results',[App\Http\Controllers\StudentExamController::class,'index'])->name('exam_result.index');
    Route::get('/student_exam/exam_id={id}',[App\Http\Controllers\StudentExamController::class,'show'])->name('exam_result.show');
    Route::get('/result',[App\Http\Controllers\StudentExamController::class,'result'])->name('exam_result.result');
    Route::get('/final',[App\Http\Controllers\StudentExamController::class,'final_result'])->name('exam_result.final');
    Route::get('/result_pdf',[App\Http\Controllers\StudentExamController::class,'generatePDF'])->name('exam_result.pdf_generate');

});

Route::group(['prefix' => 'roles'], function(){
    Route::get('/index',[App\Http\Controllers\RoleController::class,'index'])->name('roles.index');
    Route::get('/create',[App\Http\Controllers\RoleController::class,'create'])->name('roles.create');
    Route::post('/store',[App\Http\Controllers\RoleController::class,'store'])->name('roles.store');
    Route::get('/edit/{id}',[App\Http\Controllers\RoleController::class,'edit'])->name('roles.edit');
    Route::post('/update/{id}',[App\Http\Controllers\RoleController::class,'update'])->name('roles.update');
    Route::get('/delete/{id}',[App\Http\Controllers\RoleController::class,'destroy'])->name('roles.delete');
});

Route::group(['prefix' => 'permissions'], function(){
    Route::get('/index',[App\Http\Controllers\PermissionController::class,'index'])->name('permissions.index');
    Route::get('/create',[App\Http\Controllers\PermissionController::class,'create'])->name('permissions.create');
    Route::post('/store',[App\Http\Controllers\PermissionController::class,'store'])->name('permissions.store');
    Route::get('/edit/{id}',[App\Http\Controllers\PermissionController::class,'edit'])->name('permissions.edit');
    Route::post('/update/{id}',[App\Http\Controllers\PermissionController::class,'update'])->name('permissions.update');
    Route::get('/delete/{id}',[App\Http\Controllers\PermissionController::class,'destroy'])->name('permissions.delete');
});

Route::group(['prefix' => 'images'], function(){
    Route::get('/index',[App\Http\Controllers\ImagUploadController::class,'index'])->name('images.index');
    Route::get('/create',[App\Http\Controllers\ImagUploadController::class,'create'])->name('images.create');
    Route::post('/store',[App\Http\Controllers\ImagUploadController::class,'store'])->name('images.store');
    Route::get('/resizeImage',[App\Http\Controllers\ImagUploadController::class,'resizeImage'])->name('images.resize');
    Route::post('/resizeImagePost',[App\Http\Controllers\ImagUploadController::class,'resizeImagePost'])->name('images.resizeImagePost');
});

Route::get('/phone_verify',[App\Http\Controllers\AuthController::class, 'index']);
// Route::get('email-test', function(){
  
// 	$details['email'] = 'poojapatridhyatech@gmail.com';
  
//     dispatch(new App\Jobs\SendEmailJob($details));
  
//     dd('done');
// });