@extends('layouts.master')
@section('content')
<div id="main">
        <div class="row">
            <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Counter</span></h5>
                        </div>
                        <div class="col s12 m6 l6 right-align-md">
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Counter
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container">
				<button data-target="modal1" class="btn modal-trigger">Modal</button>
			</div>

            <div class="counter"></div>


            <div id="modal1" class="modal modal-fixed-footer ">
                <div class="modal-content">
                  <h4 class="text-center">Do you want to participate?</h4>
                  <img src="{{asset('images/gallery/48.jpg')}}" height="84%" width="100%">
                </div>
                <div class="modal-footer">
                   <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">No</a>
                  <a href="" class="modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
                </div>
            </div>

	</div>
</div>
<script src="/path/to/cdn/jquery.min.js"></script>
<script src="/path/to/js/jquery.rollNumber.js"></script>
<script>
    $(.counter).rollNumber({
        number:12345
    });
</script>
@endsection
