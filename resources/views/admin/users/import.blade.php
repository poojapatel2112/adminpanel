@extends('layouts.master')
@section('content')
<div id= "main">
	<div class="row">
		<div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Users List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('index')}}">User</a>
                            </li>
                            <li class="breadcrumb-item active">Users Import
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
		<div class="col s12">
			<div class="container">
			    <div class="card bg-light mt-3">
			        <div class="card-header">
			           <h5 class="breadcrumbs-title mt-0 mb-0 center"><span>Import Users List</span></h5>
			        </div>
			        <div class="card-body">
			            <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
			                @csrf
			               <input type="file" name="file" id="input-file-now" class="dropify" data-default-file="">
			                @if ($errors->has('file'))
                            <div class="alert alert-danger error invalid-feedback" style="color: red;" role="alert">{{ $errors->first('file') }}</div>
                            @endif
			                <br>
			                
			                
			           
			        </div>
			        <div class="card-footer">
			        	<button class="btn btn-block indigo waves-effect waves-light" type="submit">Import User Data</button>
			        </div>
			         </form>
			    </div>
			</div>
		</div>
	</div>
</div>
@endsection