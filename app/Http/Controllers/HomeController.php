<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Exam;
use app\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $total_exam = Exam::count();
        $total_user = User::where('role_id','<>','1')->count();
        if($user->role_id == 1)
        {    
           
            return view('admin.dashboard',compact('total_user','total_exam'));
        }
        else
        {
            // echo "hii";
             // return view('admin.dashboard');
            return view('students.dashboard',compact('total_user','total_exam'));
        }
    }
}
