<?php
  
namespace App\Http\Controllers\Auth;
  
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Socialite;
use Auth;
use Exception;
use App\Models\User;
use App\Models\Location;
  
class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
      
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
    
            $user = Socialite::driver('google')->user();
            $finduser = User::where('google_id', $user->id)->first();
            $findEmail = User::where('email',$user->email)->first();
            // echo '<pre>';print_r($findEmail);exit();
            // dd($findEmail);
            if($finduser){
                // echo "hii";exit();
                Auth::login($finduser);
    
                return redirect('/home');
     
            }else{
                if($findEmail)
                {
                    $findEmail->google_id = $user->id;
                    $findEmail->save();
                    Auth::login($findEmail);
     
                    return redirect('/home');
                }
                else
                {
                    $newUser = User::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'google_id'=>   $user->id,
                        'role_id' => 1,   
                        'password' => encrypt('123456dummy')
                    ]);
                // dd($newUser);
                    Auth::login($newUser);
         
                    return redirect('/home');
                }
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function index()
    {
        return view('admin.google');
    }

    public function search(Request $request)
    {
       // print_r($request->all());exit;
        $lat = $request->latitude;
        $long = $request->longitude;
            
        $locations = DB::table("locations")
            ->select("locations.*"
                ,DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                * cos(radians(locations.lat)) 
                * cos(radians(locations.long) - radians(" . $long . ")) 
                + sin(radians(" .$lat. ")) 
                * sin(radians(locations.lat))) AS distance"))
                ->having('distance', '<', 25)
                ->orderBy('distance', 'asc')
                ->get();
                // dd($data);
        return view('admin.google_search',compact('locations'));
    }

    public function store(Request $request)
    {
       // print_r($request->all());exit;
       $location = new Location();
       $location->name = $request->sublocality;
       $location->long = $request->longitude;
       $location->lat = $request->latitude;
       $location->save();
       return redirect()->back();
    }

    // id= AC5793c0c403b9fadf2f46f1e254b1eac1
    // token = 7a45f97709234c91babfd6d5a6e2a155
    // service - id = VAd52f44ca6adacee7f3f215d56fdf799d

    // google client ID : 94796700354-kgplog9np62b454rp5v4kbd4ebeqnkvh.apps.googleusercontent.com
    // client secret: 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
    // Refresh token: 1//04fNZaKPMfYoiCgYIARAAGAQSNwF-L9IrJ33l9JRQdHiiI6zj5TCDKip0D5IjflOuUzNLSvozS4HDKarAj7G-ePxO8ajFo4wVMAo
    // folder ID : 1e4PVajS9ORebSGN50h-AnKzVu3NAD2aS
}