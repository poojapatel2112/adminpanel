<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Event;

class ProductController extends Controller
{
    public function index()
    {
        $fruits = Product::where('product_type','fruit')->count();
        $vege = Product::where('product_type','vegetable')->count();
        $colours = Product::where('product_type','colour')->count();
        $products = Product::all();
        // echo '<pre>';print_r($colours);exit;
        return view('admin.chart',compact('fruits','vege','colours','products'));
    }

    public function counter()
    {
        return view('admin.counter');
    }

    public function calender(Request $request)
    {   
        
        if($request->ajax()) {
             $data = Event::whereDate('start', '>=', $request->start)
                       ->whereDate('end',   '<=', $request->end)
                       ->get(['id', 'title', 'start', 'end']);
                  
             return response()->json($data);
        }
        return view('admin.calender');
    }

    public function calenderAjax(Request $request)
    {
        
        switch ($request->type) {
           case 'add':
              $event = Event::create([
                  'title' => $request->event_name,
                  'start' => $request->event_start,
                  'end' => $request->event_end,
              ]);
 
              return response()->json($event);
             break;
  
           case 'update':
              $event = Event::find($request->id)->update([
                  'start' => $request->start,
                  'end' => $request->end,
              ]);
 
              return response()->json($event);
             break;
  
           case 'delete':
              $event = Event::find($request->id)->delete();
  
              return response()->json($event);
             break;
             
           default:
             # code...
             break;
        }
    }

    public function products(Request $request)
    {
      
        if($request->has('product_search')){
            $products = Product::search($request->product_search)
                ->paginate(7);
        }else{
            $products = Product::all();
        }
        return view('admin.products', compact('products'));
    }

    public function replicate($id)
    {
        $product = Product::find($id);
        $new_product = $product->replicate();
        $new_product->save();
        return redirect()->route('products');
    }
}
