<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student_exam extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'exam_id',
    ];

    public function exam_name()
    {
        return $this->belongsTo(Exam::class,'exam_id');
    }

    public function students()
    {
        return $this->belongsTo(User::class,'student_id');
    } 
}
