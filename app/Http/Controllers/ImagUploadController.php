<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photo;
use Image;


class ImagUploadController extends Controller
{
    public function index()
    {
    	$images = photo::all();
    	// dd($images);
        return view('admin.images.index',compact('images'));
    }

    public function create()
    {
    	return view('admin.images.create');
    }

    public function store(Request $request)
    {
    	// print_r($request->all());exit();
        $validatedData = $request->validate([
         'image' => 'required|mimes:jpg,png,jpeg,gif,svg',
        ]);
        $name = $request->file('image')->getClientOriginalName();
        $path = 'uploads/';
        $file = $request->file('image');
        $fileExtension =$file->getClientOriginalExtension();
        $fileName = time() . rand(15, 100) . '.' . $fileExtension;
        $path = $path . "/" . date('FY');
        $file->storeAs($path, $fileName, 'public');
        $save = new Photo;
        $save->name = $name;
        $save->path = $path . "/" . $fileName;
        $save->save();

        $destinationPath = public_path('/thumbnail');
        $img = Image::make($file->getRealPath());
        $img->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$save->name);


        $destinationPath = public_path('/originals');
        $file->move($destinationPath, $save->name);


        
        return redirect()->route('images.index')->with('status', 'Image Has been uploaded successfully with validation in laravel');
    }

    public function resizeImage()
    {
        return view('admin.images.resize');
    }

    public function resizeImagePost(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
     
   
        $destinationPath = public_path('/thumbnail');
        $image->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$input['imagename']);


        $destinationPath = public_path('/originals');
        $image->move($destinationPath, $input['imagename']);
        $save = new Image;
        $save->name = $input['imagename'];
        $save->path = $destinationPath;
        $save->save();

        return back()
            ->with('success','Image Upload successful')
            ->with('imageName',$input['imagename']);
    }

}
