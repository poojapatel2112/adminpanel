<?php

namespace App\Http\Controllers;

use App\Models\ModelsGroup;
use Illuminate\Http\Request;

class ModelsGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ModelsGroup  $modelsGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsGroup $modelsGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ModelsGroup  $modelsGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsGroup $modelsGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ModelsGroup  $modelsGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsGroup $modelsGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ModelsGroup  $modelsGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsGroup $modelsGroup)
    {
        //
    }
}
