<?php

namespace App\Http\Controllers;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\ModelsGroup;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Role::select();
        if(isset($request->name_search) && !empty($request->name_search))
        {
            $data->where('name','like', '%'.$request->name_search.'%');
        }
        if(isset($request->slug_search) && !empty($request->slug_search))
        {
            $data->where('guard_name','like', '%'.$request->slug_search.'%');
        }
        $roles = $data->get();
        return view('admin.roles.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::pluck('name','id')->all();
        $role = Role::pluck('name','id')->all();
        // echo '<pre>';print_r($permission);exit();
        return view('admin.roles.create',compact('permission','role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        print_r($request->all());exit();
        $validator=Validator::make($request->all(),[
                    'role_slug'=>'required',
                    'role_name' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::to('roles/create')
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $role = new Role();
            $role->name = $request->role_name;
            $role->guard_name = $request->role_slug;
            $role->save();

            // $role_permission = new Role_has
            alert()->info('Role Added Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('roles.index'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('admin.roles.edit',compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
                    'role_slug'=>'required',
                    'role_name' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $role = Role::find($id);
            $role->name = $request->role_name;
            $role->guard_name = $request->role_slug;
            $role->save();
            alert()->info('Role Updated Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('roles.index'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $role = Role::find($id);
        $role->delete();
        if($role){
            return response([
            'status'  => 'success',
            ]);
        }
        return redirect()->route('roles.index');
    }
}
