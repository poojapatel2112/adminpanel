<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Question;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::all();
        return view('admin.exams.index',compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.exams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r($request->all());exit();
        $validator=Validator::make($request->all(),[
                    'exam_name' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::to('exams/create')
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $exam = new Exam();
            $exam->name = $request->exam_name;
            $exam->save();

            // $role_permission = new Role_has
            alert()->info('Exam Added Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('exams.index'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::find($id);
        return view('admin.exams.edit',compact('exam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
                    'exam_name' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::to('exams/create')
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $exam = Exam::find($id);
            $exam->name = $request->exam_name;
            $exam->save();

            // $role_permission = new Role_has
            alert()->info('Exam Update Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('exams.index'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::find($id);
        $exam->delete();
        if($exam){
            return response([
            'status'  => 'success',
            ]);
        }
        return redirect()->route('exams.index');

    }

    public function question($id)
    {
        $exam_id = $id;
        // dd($exam_id);
        // return view('admin.questions.create',compact('exam_id'));
        $questions = Question::where('exam_id',$exam_id)->get();
        return view('admin.questions.index',compact('questions','exam_id'));
    }
}
