<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Google Autocomplete Address Example</title>
    
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>

    <style type="text/css">
        #map {
            width: 100%;
            height: 400px;
        }
        .mapControls {
            margin-top: 10px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }
        #searchMapInput {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 50%;
        }
        #searchMapInput:focus {
            border-color: #4d90fe;
        }
    </style>
</head>
@extends('layouts.master')
@section('content')
<div id="main">
        <div class="row">
            <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Google Map</span></h5>
                        </div>
                        <div class="col s12 m6 l6 right-align-md">
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Google Map
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col s12">
                <div class="container">
                    <div class="row ">
                        <label>Location/City/Address</label>
                        <input type="text" name="autocomplete" id="autocomplete" class="col s12 input-field" placeholder="Choose Location" >
                    </div>
                    <div class="row">
                        <div class="col s12 m4 l6">
                            <label>sub locality</label>
                            <input type="text" id="sublocality" name="latitude" class="form-control">
                        </div>
                        <div class="col s12 m4 l6">
                            <label>locality</label>
                            <input type="text" id="locality" name="latitude" class="form-control">
                        </div>
                        <div class="col s12 m4 l6">
                            <label>city</label>
                            <input type="text" id="city" name="latitude" class="form-control">
                        </div>
                        <div class="col s12 m4 l6" >
                            <label>state</label>
                            <input type="text" id="state" name="latitude" class="form-control">
                        </div>
                        <div class="col s12 m4 l6" >
                            <label>country</label>
                            <input type="text" id="country" name="latitude" class="form-control">
                        </div>
                        <div class="col s12 m4 l6" >
                            <label>pin code</label>
                            <input type="text" id="pincode" name="latitude" class="form-control">
                        </div>
                    </div>
              
                    <div class="col s12 m4 l6" id="latitudeArea">
                        <label>Latitude</label>
                        <input type="text" id="latitude" name="latitude" class="form-control">
                    </div>
              
                    <div class="col s12 m4 l6" id="longtitudeArea">
                        <label>Longitude</label>
                        <input type="text" name="longitude" id="longitude" class="form-control">
                    </div>
                </div>
            </div> -->
        </div>
   

   <!--  <div id="mymap" class="container mt-2"></div> -->
    <input id="searchMapInput" class="mapControls container mt-2" type="text" placeholder="Enter a location">
    <div id="map"></div>
    <ul id="geoData">
        <li>Full Address: <span id="location-snap"></span></li>
        <!-- <li>Latitude: <span id="lat-span"></span></li>
        <li>Longitude: <span id="lon-span"></span></li> -->
    </ul>
    <div class="row">
        <div class="col s12 m4 l6">
            <label>sub locality</label>
            <input type="text" id="" name="latitude" class="form-control sublocality">
        </div>
        <div class="col s12 m4 l6">
            <label>locality</label>
            <input type="text" id="" name="latitude" class="form-control locality">
        </div>
        <div class="col s12 m4 l6">
            <label>city</label>
            <input type="text" id="city" name="latitude" class="form-control">
        </div>
        <div class="col s12 m4 l6" >
            <label>state</label>
            <input type="text" id="state" name="latitude" class="form-control">
        </div>
        <div class="col s12 m4 l6" >
            <label>country</label>
            <input type="text" id="country" name="latitude" class="form-control">
        </div>
        <div class="col s12 m4 l6" >
            <label>pin code</label>
            <input type="text" id="pincode" name="latitude" class="form-control">
        </div>
    </div>
    <form action="{{route('map.search')}}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <input type="hidden" id="" name="sublocality" class="form-control sublocality">
            <div class="col s12 m4 l6" id="latitudeArea">
                <label>Latitude</label>
                <input type="text" id="latitude" name="latitude" class="form-control latitude">
            </div>
            
            <div class="col s12 m4 l6" id="longtitudeArea">
                <label>Longitude</label>
                <input type="text" name="longitude" id="longitude" class="form-control longitude">
            </div>
        </div>
        <button class="btn btn-light ml-2  mb-2" id="submit">Store</button>
        
    </form>

    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
    <script type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=AIzaSyBXvkATHldPxhN0OAKyUkMbqZTx3WVreQ8 &libraries=places" ></script>

    <!-- <script>
        $(document).ready(function () {
            $("#latitudeArea").addClass("d-none");
            $("#longtitudeArea").addClass("d-none");
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', initialize);
        

        function initialize() {
            var mymap = new GMaps({
                  el: '#mymap',
                  lat: 22.3038945,
                  lng: 70.80215989999999,
                  zoom:6
                });
            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();

                $('#latitude').val(place.geometry['location'].lat());
                $('#longitude').val(place.geometry['location'].lng());
                for (var i = 0; i < place.address_components.length; i++) {
                    if(place.address_components[i].types[0] == "sublocality_level_1")
                    {
                        var sub_local = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "locality")
                    {
                        var local = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "administrative_area_level_2")
                    {
                        var city = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "administrative_area_level_1")
                    {
                        var state = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "country")
                    {
                        var country = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "postal_code")
                    {
                        var pincode = place.address_components[i].long_name ;
                    }
                    var title = place.vicinity;

                }
                $('#sublocality').val(sub_local);
                $('#locality').val(local);
                $('#city').val(city);
                $('#state').val(state);
                $('#country').val(country);
                $('#pincode').val(pincode);

                // var lat = $('#latitude').val();
                // var lan = $('#longitude').val();
                // var title = $('#autocomplete').val();

                var lat = $('#latitude').val() ? $('#latitude').val() : 22.3038945;
                var lng = $('#longitude').val() ? $('#longitude').val() : 70.80215989999999;
                // var title = 
                mymap.addMarker({
                  lat: lat,
                  lng:lng,
                  title: title,
                });
              
                
            });

        }

        function initMap() {
          const myLatlng = { lat: -25.363, lng: 131.044 };
          const map = new google.maps.Map(document.getElementById("mymap"), {
            zoom: 4,
            center: myLatlng,
          });
          // Create the initial InfoWindow.
          let infoWindow = new google.maps.InfoWindow({
            content: "Click the map to get Lat/Lng!",
            position: myLatlng,
          });

          infoWindow.open(map);
          // Configure the click listener.
          map.addListener("click", (mapsMouseEvent) => {
            // Close the current InfoWindow.
            infoWindow.close();
            // Create a new InfoWindow.
            infoWindow = new google.maps.InfoWindow({
              position: mapsMouseEvent.latLng,
            });
            infoWindow.setContent(
              JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
            );
            infoWindow.open(map);
          });
        }

    </script> -->
    <script>
    google.maps.event.addDomListener(window, 'load', initMap);
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 23.0484612, lng: 72.5300956},
          zoom: 13
        });
        var input = document.getElementById('searchMapInput');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
       
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
      
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });
      
        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
        
            /* If the place has a geometry, then present it on a map. */
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setIcon(({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
          
            var address = '';
            if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            for (var i = 0; i < place.address_components.length; i++) {
                    if(place.address_components[i].types[0] == "sublocality_level_1")
                    {
                        var sub_local = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "locality")
                    {
                        var local = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "administrative_area_level_2")
                    {
                        var city = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "administrative_area_level_1")
                    {
                        var state = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "country")
                    {
                        var country = place.address_components[i].long_name ;
                    }
                    if(place.address_components[i].types[0] == "postal_code")
                    {
                        var pincode = place.address_components[i].long_name ;
                    }
                    var title = place.vicinity;

                }
                $('.sublocality').val(sub_local);
                $('.locality').val(local);
                $('#city').val(city);
                $('#state').val(state);
                $('#country').val(country);
                $('#pincode').val(pincode);
                $('.latitude').val(place.geometry['location'].lat());
                $('.longitude').val(place.geometry['location'].lng());

          
            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);
            
            /* Location details */
            document.getElementById('location-snap').innerHTML = place.formatted_address;
            // document.getElementById('latitude').innerHTML = place.geometry.location.lat();
            // document.getElementById('longitude').innerHTML = place.geometry.location.lng();
        });
    }

    
</script>
@endsection