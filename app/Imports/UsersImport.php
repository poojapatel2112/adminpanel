<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\User;
use App\Jobs\SendEmailJob;
use Maatwebsite\Excel\Concerns\WithStartRow;

use Maatwebsite\Excel\Concerns\Importable;
use Alert;

class UsersImport implements ToModel,WithStartRow
{
    /**
    * @param Collection $collection
    */
    use Importable;

    
    public function startRow(): int {return 2; }



    public function model(array $row)
    {

        if(!empty($row[0])){
            $userIsAvailable = User::where('email',$row[1])->count();

            if($userIsAvailable > 0)
            {
                alert()->info($row[1].' already in use.')->persistent('close')->autoclose("3600"); 
                // continue;
            }
            else
            {

                $user = new User();
                $user->name = $row[0];
                $user->email = $row[1];
                $user->role_id = $row[2];
                $user->password = \Hash::make($row[3]);
                $user->phone_number = $row[4];
                $user->save();
                // $details = [];
                // $details['email'] = $user->email;
                // $details['name'] = $user->name;
                // echo '<pre>';print_r($details);
                // exit();

                // SendEmailJob::dispatch($details);
            }
        }
    }
}
