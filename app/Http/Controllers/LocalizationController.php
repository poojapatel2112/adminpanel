<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\App;


class LocalizationController extends Controller
{
    public function switchLang($lang)
    {
    	// print_r($lang);exit();
        App::setLocale($lang);
        session()->put('locale', $lang);
        // alert(session('locale'));
        // echo trans('sentence.user_list');exit();
        return redirect()->back();
    }
}
