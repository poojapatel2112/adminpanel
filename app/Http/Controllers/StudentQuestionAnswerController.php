<?php

namespace App\Http\Controllers;

use App\Models\Student_question_answer;
use App\Models\Student_exam;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Answer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;
use Alert;

class StudentQuestionAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::all();
        return view('students.exams',compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function questions($id)
    {
        
        $questions = Question::with('answer')->where('exam_id',$id)->get();
        // echo '<pre>';print_r($questions);exit;
        return view('students.questions',compact('questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo '<pre>';print_r($request->all());exit;
        $validator=Validator::make($request->all(),[
                    'exam_id' => 'required',
                    'que_id' => 'required',
                    'ans_id'=> 'required',
                    
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return response()->json(['status'=>false,"error"=>$validator->errors()]);
        }
        else{
            $user = Auth::user();
            // $total_que = 0;
            $que_count = Question::where('exam_id',$request->exam_id)->count();
            if(!empty($request->stu_exam_id))
            {
                $stu_exam_id = $request->stu_exam_id;
                $total_que = $request->total_que;
            }
            else
            {    
                
                // dd($que_count);
                $stu_exam = new Student_exam();
                $stu_exam->student_id = $user->id;
                $stu_exam->exam_id = $request->exam_id;
                $stu_exam->status = 0;
                $stu_exam->total_mark = $que_count;
                $stu_exam->save();
                $stu_exam_id = $stu_exam->id;
                $total_que = 0;
            }

            $stu_que_ans= new Student_question_answer();
            $stu_que_ans->student_exams_id = $stu_exam_id;
            $stu_que_ans->question_id = $request->que_id;
            $stu_que_ans->status = 0;
            if(!empty($request->value))
            {
                $answer = implode(',',$request->value);
            }
            else
            {
                $answer = $request->ans_id;
            }
            // print_r($list);exit;
            $stu_que_ans->answer = $answer;
            $stu_que_ans->save();
            $total_que++;
            // print_r($total_que);
            if($total_que == $que_count)
            {
                return response()->json(['status'=>'completed','redirect_url' => route('students.exams'),'total_que' => $total_que]);
            }
            else
            {
                return response()->json(['status'=>'success','id'=>$stu_exam_id,'total_que' => $total_que]);
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student_question_answer  $student_question_answer
     * @return \Illuminate\Http\Response
     */
    public function show(Student_question_answer $student_question_answer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student_question_answer  $student_question_answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Student_question_answer $student_question_answer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student_question_answer  $student_question_answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student_question_answer $student_question_answer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student_question_answer  $student_question_answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student_question_answer $student_question_answer)
    {
        //
    }

    public function result()
    {
        $user = Auth::user();
        $exam_list = Student_exam::with('exam_name')->where('student_id',$user->id)->get();
        // echo '<pre>';print_r($exam_list);exit;
        return view('students.result',compact('exam_list'));
    }
}
