@extends('layouts.master')
@section('content')
<div id="main">
        <div class="row">
            <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Question Add</span></h5>
                        </div>
                        <div class="col s12 m6 l6 right-align-md">
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('exams.index')}}">Exam</a>
                                </li>
                                <li class="breadcrumb-item active">Exam Add
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users edit start -->
                    <div class="section users-edit">
                        <div class="card">
                            <div class="card-content">
                                <!-- <div class="card-body"> -->
                                
                                <div class="row">
                                    <div class="col s12" id="account">
                                        <!-- users edit media object start -->
                                        <!-- users edit account form start -->
                                        <form id="accountForm" method="post" action="{{route('questions.store')}}">
                                        	{{ csrf_field() }}
                                            <input id="question" name="exam_id" type="hidden" class="validate" value="{{$exam_id}}" >
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="question" name="question" type="text" class="validate" value="{{ old('question') }}">
                                                            <label for="question">Question</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('question') }}</div>
                                                        </div>

                                                        
                                                    </div>
                                                </div>

                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <select name="question_type" class="question_type">
                                                                <option value="" disabled selected>select question type</option>
                                                                <option value="1" {{ old('question_type') == 1 ? "selected" : "" }}>Sigle Choice</option>
                                                                <option value="2" {{ old('question_type') == 2 ? "selected" : "" }}>Multiple Choice</option>
                                                                <option value="3" {{ old('question_type') == 3 ? "selected" : "" }}>Text</option>
                                                                <option value="4" {{ old('question_type') == 4 ? "selected" : "" }}>Date</option>
                                                                <option value="5" {{ old('question_type') == 5 ? "selected" : "" }}>Text Area</option>
                                                            </select>
                                                            <label>Question Type</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('question_type') }}</div>
                                                        </div> 
                                                    </div>
                                                </div>

                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <label class="ml-5 input-field" for="question">Is Mandatory? </label>
                                                        <div class="col s12 input-field">
                                                            <div class="switch mb-1">
                                                                <label>
                                                                    No
                                                                    <input checked type="checkbox" name="is_required">
                                                                    <span class="lever"></span>
                                                                    Yes
                                                                </label>
                                                            </div>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('is_required') }}</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col s12 answer multiple">
                                                    <div class="row" id="addField">
                                                        <div class="col s12 m3 input-field" >
                                                            <input id="question" name="answer[]" type="text" class="validate" value="{{ old('answer') }}"><a class="addmore waves-effect btn-flat right" id="addmore"><i class="material-icons">add</i></a>
                                                            <label for="question">Answer</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('answer') }}</div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="col s12 display-flex justify-content-end mt-3">
                                                    <button type="submit" class="btn indigo">
                                                        Save Exam</button>
                                                    <a href="{{route('exams.index')}}" class="btn btn-light"  >Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- users edit ends -->
                    <!-- START RIGHT SIDEBAR NAV -->
                    
                    <!-- END RIGHT SIDEBAR NAV -->
                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                        <ul>
                            <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                            <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                            <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                            <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
@endsection