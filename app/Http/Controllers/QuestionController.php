<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Exam;
use App\Models\Answer;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('exams')->get();
        // dd($questions);
        return view('admin.questions.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $exam_id = $id;
        // dd($exams);
        return view('admin.questions.create',['exam_id' => $exam_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator=Validator::make($request->all(),[
                    'exam_id' => 'required',
                    'question' => 'required',
                    'is_required' => 'required',
                    'answer' => 'required_if:question_type,=,1,2',
                    'question_type' => 'required'
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        }
        else{
            if($request->is_required == "on")
            {
                $is_required = 1;
            }
            else
            {
                $is_required = 0;
            }
            // dd($list);
            $question = new Question();
            $question->exam_id = $request->exam_id;
            $question->question = $request->question;
            $question->question_type = $request->question_type;
            $question->is_mandatory = $is_required;
            $question->save();
            if($question->question_type == 1 || $question->question_type == 2)
            {
                foreach (($request->answer) as $key => $val) {
                        // echo '<pre>';print_r($val);
                        $answer = new Answer();
                        $answer->question_id=$question->id;
                        $answer->answer= $val;
                        $answer->save();

                }

            }
            
            alert()->info('Question Added Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('exams.index'); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);
        $answer = Answer::where('question_id',$question->id)->get();
        // dd($question);
        return view('admin.questions.edit',compact('question','answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validator=Validator::make($request->all(),[
                    'exam_id' => 'required',
                    'question' => 'required',
                    'is_required' => 'required',
                    'answer' => 'required_if:question_type,=,1,2',
                    'question_type' => 'required'
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        }
        else{
            if($request->is_required == "on")
            {
                $is_required = 1;
            }
            else
            {
                $is_required = 0;
            }
            $question = Question::find($id);
            $question->exam_id = $request->exam_id;
            $question->question = $request->question;
            $question->question_type = $request->question_type;
            $question->is_mandatory = $is_required;
            $question->save();
            $delete_answer = Answer::where('question_id',$question->id)->delete();
            // echo "delete";exit;
            if($question->question_type == 1 || $question->question_type == 2)
            {
                foreach (($request->answer) as $key => $val) {
                        // echo '<pre>';print_r($val);
                        $answer = new Answer();
                        $answer->question_id=$question->id;
                        $answer->answer= $val;
                        $answer->save();
                }
            }
            
            alert()->info('Question Updated Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('exams.index'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::where('question_id',$id)->delete();
        $question = Question::find($id);
        $question->delete();
        if($question){
            return response([
            'status'  => 'success',
            ]);
        }
        return redirect()->route('exams.index');
    }
}
