@extends('layouts.master')
@section('content')

<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Question List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('students.exams')}}">Exam</a>
                            </li>
                            <li class="breadcrumb-item active">Question
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="card-header">
                    </div>
                
                    <ul class="stepper" id="nonLinearStepper">
                        @php $i=1; @endphp
                        @foreach($questions as $question)
                        @if($loop->index == 0)
                        <li class="step active" id="step_{{$i}}">
                        @else
                        <li class="step" id="step_{{$i}}">
                        @endif
                                <div class="step-title waves-effect">Question {{$i}}</div>
                                    <div class="step-content">
                                        <div class="row">
                                            <div class="input-field col m6 s12">
                                                <label for="firstName1">{{$question->question}} <span class="red-text">*</span></label>
                                                <input type="hidden" value="{{$question->id}}" name="que_id" id="que_id_{{$i}}" class="que_id">
                                                <input type="hidden" value="{{$question->exam_id}}" name="exam_id" id="exam_id">
                                                <input type="hidden" value="" name="stu_exam_id" id="stu_exam_id">
                                                <input type="hidden" value="" name="total_que" id="total_que">
                                               
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col s12 mt-3">
                                                @if($question->question_type == 1)
                                                        @foreach($question->answer as $ans)
                                                        <label for="answer_{{$ans->id}}">
                                                        
                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}" type="radio" value="{{$ans->id}}" class="ans_id" data-attr="radio" required />
                                                            <span>{{$ans->answer}}</span>
                                                        </label>
                                                        @endforeach
                                                @endif
                                                @if($question->question_type == 2)
                                                        @foreach($question->answer as $ans)
                                                        <label for="answer_{{$ans->id}}">
                                                        
                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}" type="checkbox" value="{{$ans->id}}" data-attr="checkbox" required />
                                                            <span>{{$ans->answer}}</span>
                                                        </label>
                                                        @endforeach
                                                @endif
                                                @if($question->question_type == 3)
                                                        <label for="">
                                                        
                                                            <input id="answer_text" name="answer_{{$i}}" type="text" value="" class="col s12 m3 input-field"/>
                                                            
                                                        </label>
                                                       
                                                @endif
                                                @if($question->question_type == 4)
                                                        <label for="">
                                                        
                                                            <input id="answer_date" name="answer_{{$i}}" type="date" value="" class="col s12 m3 input-field"/>
                                                        </label>
                                                        <span class="errorTxt1 error invalid-feedback"></span>
                                                       
                                                @endif
                                                @if($question->question_type == 5)
                                                        <label for="">
                                                        
                                                            <input id="answer_text" name="answer_{{$i}}" type="text" value="" class="col s12 m3 input-field" />
                                                            
                                                        </label>
                                                       
                                                @endif
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="step-actions">
                                            <div class="row">
                                                <div class="col m4 s12 mb-3">
                                                    <button class="red btn btn-reset" type="reset">
                                                        <i class="material-icons left">clear</i>Reset
                                                    </button>
                                                </div>
                                                <div class="col m4 s12 mb-3">
                                                    <button class="btn btn-light previous-step pre_btn" id="pre_btn" data-attr={{$i}}>
                                                        <i class="material-icons left">arrow_back</i>
                                                        Prev
                                                    </button>
                                                </div>
                                                <div class="col m4 s12 mb-3">
                                                    <button class="waves-effect waves dark btn btn-primary next-step next_btn" type="submit" id="next_btn" name="next_btn" data-url ="{{route('students.store')}}" data-attr={{$i}}>
                                                        @if($loop->last)
                                                            Submit
                                                        @else
                                                            Next
                                                        @endif
                                                        <i class="material-icons right">arrow_forward</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </li>
                        @php $i++; @endphp
                        @endforeach
                    </ul>     
                </div>
            </div>
        </div>    
    </div>
</div>

   
@endsection