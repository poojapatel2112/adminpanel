<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test No.</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        html {
            margin: 0;
        }
        body {
            background-color: #FFFFFF;
            font-size: 10px;
            margin: 36pt;
        }
        .tnc_select1{
            margin-top: 5px;
        }


    
    </style>
</head>
<body>
<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h2><span><center>Exam Result</center></span></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="row gradient-45deg-blue-indigo padding-1 medium-small">
                        <div class="col s12 m4 users-view-timeline">
                            <h4>Total Mark: <span class="white-text">{{$student_exam->total_mark}}</span></h4>
                        
                            <h4 >Obtain Mark : <span class="white-text">{{$student_exam->obtain_mark}}</span></h4>
        
                            @if($student_exam->status == 1)
                                <h4 >Status: <span class="white-text">PASS</span></h4>
                            @elseif($student_exam->status == 2)
                                <h4 >Status: <span class="white-text">fAIL</span></h4>
                            @else
                                <h4>Status: <span class="white-text">PENDING</span></h4>
                            @endif
                        </div>
                    </div>
                    <ul class="stepper" id="nonLinearStepper">
                        @php $i=1; @endphp
                        @foreach($stu_que_ans as $question)
                        
                        <li class="step active" id="step_{{$i}}">
                                <div class="step-title waves-effect">Question {{$i}}
                                	<label class="right">
                                        <input class=" validate_{{$question->id}} tnc_select1" id="tnc_select1{{$i}}" type="checkbox" value="1" {{ $question->status == '1' ? 'checked' : '' }} name="mark" data-url="{{route('exam_result.result')}}" data-attr="{{$question->id}}"/>
                                        <span></span>
                                    </label>
                                </div>
                                    <div class="step-content">
                                        <div class="row">
                                            <div class="input-field col m6 s12">
                                                <label for="firstName1">Q: {{$question->questions->question}} <span class="red-text">*</span></label>
                                                <input type="hidden" value="{{$question->id}}" name="que_id" id="que_id_{{$i}}" class="que_id">
                                                <input type="hidden" value="{{$question->exam_id}}" name="exam_id" id="exam_id">
                                                <input type="hidden" value="{{$question->student_exams_id}}" name="stu_exam_id" id="stu_exam_id">
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col s12 mt-3">
                                             	@foreach($stu_questions as $que)
                                                @if($question->questions->question_type == 1)
                                                        @if($question->questions->id == $que->id)
                                                        
                                                            <label for="">
                                                                <span >Ans: {{$question->ans->answer}}</span>
                                                            </label>
                                                        
                                                        @endif
                                                       
                                                @endif
                                                @if($question->questions->question_type == 2)
                                                  
                                                @php $array_ans = explode(',',$question->answer); @endphp

                                                       @if($question->questions->id == $que->id)
                                                        @foreach($que->answer as $ans)
                                                            <label for="answer_">
                                                            
                                                                @if(in_array($ans->id,$array_ans))
                                                                <span>Ans: {{$ans->answer}}</span>
                                                                @endif
                                                            </label>
                                                        @endforeach
                                                    @endif
                                                @endif
                                                @if($question->questions->question_type == 3)
                                                        @if($question->questions->id == $que->id)
                                                    	
	                                                        <label for="">
	                                                        
                                                                <span>Ans: {{$question->answer}}</span>
	                                                        </label>
                                                   		
                                                   	@endif
                                                       
                                                @endif
                                                @if($question->questions->question_type == 4)
                                                       @if($question->questions->id == $que->id)
                                                    	
	                                                        <label for="answer_">
	                                                        
	                                                            <span>Ans: {{$question->answer}}</span>
	                                                        </label>
                                                   		
                                                   	@endif
                                                       
                                                @endif
                                                @if($question->questions->question_type == 5)
                                                      @if($question->questions->id == $que->id)
                                                    
	                                                        <label for="answer_">
	                                                        
	                                                            <span>Ans: {{$question->answer}}</span>
	                                                           
	                                                        </label>
                                                   		
                                                   	@endif
                                                       
                                                @endif
                                                 @endforeach
                                            </div>
                                            
                                        </div>

                                    </div>
                        </li>
                        @php $i++; @endphp
                        @endforeach
                    </ul>
                </div>
                
            </div>
        </div>    
    </div>
</div>
</body>
</html>