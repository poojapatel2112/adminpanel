@extends('auth.layouts.auth')

@section('content')
<div class="container">
    <div id="login-page" class="row">
        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
            <form class="login-form"  method="POST" action="{{ route('login.unlock') }}">
                @csrf
                <div class="row">
                    <div class="input-field col s12">
                        <h5 class="ml-4">Locked</h5>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <i class="material-icons prefix pt-2">lock_outline</i>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                         <label for="password" class="center-align">Password</label>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="password"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <!-- <a href="" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">Login</a> -->
                        <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">
                                    {{ __('Unlock') }}
                        </button>
                    </div>

                </div>
                 <a href="{{ url('auth/google') }}" style="margin-top: 20px;" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12"><i class="fab fa-google"></i>
                  <strong>Login With Google</strong>
                </a> 
                <div class="row">
                    <div class="input-field col s6 m6 l6">
                        <p class="margin medium-small"><a href="{{ route('register') }}">Register Now!</a></p>
                    </div>
                    <div class="input-field col s6 m6 l6">
                        <p class="margin right-align medium-small"><a href="user-forgot-password.html">Forgot password ?</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
