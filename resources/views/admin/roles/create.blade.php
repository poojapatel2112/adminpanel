@extends('layouts.master')
@section('content')
<div id="main">
        <div class="row">
            <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>Role Add</span></h5>
                        </div>
                        <div class="col s12 m6 l6 right-align-md">
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Role</a>
                                </li>
                                <li class="breadcrumb-item active">Role Add
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users edit start -->
                    <div class="section users-edit">
                        <div class="card">
                            <div class="card-content">
                                <!-- <div class="card-body"> -->
                                
                                <div class="row">
                                    <div class="col s12" id="account">
                                        <!-- users edit media object start -->
                                        <!-- users edit account form start -->
                                        <form id="accountForm" method="post" action="{{route('roles.store')}}">
                                        	{{ csrf_field() }}
                                            <div class="row">
                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="role_name" name="role_name" type="text" class="validate" value="{{ old('role_name') }}" onkeydown="if(['Space'].includes(arguments[0].code)){return false;}">
                                                            <label for="rolename">Role Name</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('role_name') }}</div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="role_slug" name="role_slug" type="text" class="validate" value="{{ old('role_slug') }}" data-error=".errorTxt2" onkeydown="if(['Space'].includes(arguments[0].code)){return false;}">
                                                            <label for="roleslug">Role Slug</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('role_slug') }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <table class="mt-1">
                                                        <thead>
                                                            <tr>
                                                                <th>Module Permission</th>
                                                                <th>View</th>
                                                                <th>Add</th>
                                                                <th>Update</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($role as $role)
                                                            <tr>
                                                                    <td>
                                                                        {{$role}}
                                                                    </td>
                                                                @foreach($permission as $key => $val)
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" name="Permission[]"  value="{{$key}}" /> <!-- {{$val}} -->
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                @endforeach
                                                                <!-- <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td> -->
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    <!-- </div> -->
                                                </div>
                                                <div class="col s12 display-flex justify-content-end mt-3">
                                                    <button type="submit" class="btn indigo">
                                                        Save role</button>
                                                    <a href="{{route('roles.index')}}" class="btn btn-light"  >Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- users edit ends -->
                    <!-- START RIGHT SIDEBAR NAV -->
                    
                    <!-- END RIGHT SIDEBAR NAV -->
                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                        <ul>
                            <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                            <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                            <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                            <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
@endsection