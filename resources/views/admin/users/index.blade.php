@extends('layouts.master')
@section('content')

<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Users List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">User</a>
                            </li>
                            <li class="breadcrumb-item active">Users List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <section class="users-list-wrapper section">
                    <div class="users-list-filter">
                        <div class="card-panel">
                            <div class="row">
                                <form>
                                    <div class="col s12 m6 l3">
                                        <label for="users-list-verified">Name</label>
                                        <div class="input-field">
                                            <input type="text" name="name_search">
                                        </div>
                                    </div>
                                    <div class="col s12 m6 l3">
                                        <label for="users-list-role">Email</label>
                                        <div class="input-field">
                                            <input type="text" name="email_search">
                                        </div>
                                    </div>
                                    <div class="col s12 m6 l3 display-flex align-items-center right show-btn">
                                        <button type="submit" class="btn btn-block indigo waves-effect waves-light">Show</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                            <a href="{{ route('importExportView') }}" class="btn indigo waves-effect waves-light right mb-2">Import User Data</a>&nbsp;
                            <a class="btn right mr-2 mb-2 indigo waves-effect waves-light" href="{{ route('export') }}">Export User Data</a>
                            <a class="btn right mr-2 mb-2 indigo waves-effect waves-light" href="{{ route('users.create') }}">Add User</a>
                                <!-- datatable start -->
                                @if (count($users)>0)  
                                <div class="responsive-table">
                                    <table id="users-list1" class="table users-list-datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th> {{ __('sentence.username')}}</th>
                                                <th>{{ __('sentence.email') }}</th>
                                                <th>{{ __('sentence.edit') }}</th>
                                                <th>{{ __('sentence.delete') }}</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                                <tr>
                                                
                                                    <td>{{$user->id}}</td>
                                                    <td>{{$user->name}}</td> 
                                                    <td>{{$user->email}}</td>
                                                    <td><a href="#"><i class="material-icons">edit</i></a></td> 
                                                    <td><a href="" data-toggle="tooltip" data-placement="top"  data-original-title="Delete"  class="deleteUser" data-id="{{$user->id}}" data-attr="{{$user->id}}" data-url ="{{route('users.delete',$user->id)}}" ><i class="material-icons">delete</i></a></td>
                                                        <!-- <a href="#"><i class="material-icons">delete</i></a></td> -->
                                                </tr>
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                                <!-- datatable ends -->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- users list ends -->
                <!-- START RIGHT SIDEBAR NAV -->

        
                <!-- END RIGHT SIDEBAR NAV -->
                <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                    <ul>
                        <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                        <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                        <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                        <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                    </ul>
                </div>
            </div>
            <div class="content-overlay"></div>
        </div>
    </div>
</div>

@endsection