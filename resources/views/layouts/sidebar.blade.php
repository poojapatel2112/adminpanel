<!-- BEGIN: SideNav-->

@if((auth::user()->role_id) == 1)
    <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark gradient-45deg-deep-purple-blue sidenav-gradient sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down " src="{{asset('images/logo/materialize-logo.png')}}" alt="materialize logo" /><img class="show-on-medium-and-down hide-on-med-and-up" src="{{asset('images/logo/materialize-logo-color.png')}}" alt="materialize logo" /><span class="logo-text hide-on-med-and-down">Materialize</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li class="active bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('home')}}"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>  
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('index')}}"><i class="material-icons">face</i><span class="menu-title" data-i18n="User">User</span></a>
                <div class="collapsible-body">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a href="{{route('index')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="List">List</span></a>
                        </li>
                        <li><a href="page-users-view.html"><i class="material-icons">radio_button_unchecked</i><span data-i18n="View">View</span></a>
                        </li>
                        <li><a href="page-users-edit.html"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Edit">Edit</span></a>
                        </li>
                    </ul>
                </div>
            </li>  
           <!--  <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('roles.index')}}"><i class="material-icons">content_paste</i><span class="menu-title" data-i18n="User">Roles</span></a>
            </li> -->
           <!--  <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('permissions.index')}}"><i class="material-icons">content_paste</i><span class="menu-title" data-i18n="User">Permissions</span></a>
            </li> -->
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('images.index')}}"><i class="material-icons">image</i><span class="menu-title" data-i18n="User">Images</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('exams.index')}}"><i class="material-icons">assignment</i><span class="menu-title" data-i18n="User">Exam Type</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('exam_result.index')}}"><i class="material-icons">help_outline</i><span class="menu-title" data-i18n="User">Exam Result</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('map')}}"><i class="material-icons">add_location</i><span class="menu-title" data-i18n="User">Map</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('chart')}}"><i class="material-icons">insert_chart</i><span class="menu-title" data-i18n="User">Chart</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('counter')}}"><i class="material-icons">insert_chart</i><span class="menu-title" data-i18n="User">Counter</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('calender')}}"><i class="material-icons">today</i><span class="menu-title" data-i18n="User">Calender</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('products')}}"><i class="material-icons">today</i><span class="menu-title" data-i18n="User">Products</span></a>
            </li>
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
@else
    <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark gradient-45deg-deep-purple-blue sidenav-gradient sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down " src="{{asset('images/logo/materialize-logo.png')}}" alt="materialize logo" /><img class="show-on-medium-and-down hide-on-med-and-up" src="{{asset('images/logo/materialize-logo-color.png')}}" alt="materialize logo" /><span class="logo-text hide-on-med-and-down">Materialize</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li class="active bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('home')}}"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>  
            </li>
           <!--  <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('roles.index')}}"><i class="material-icons">content_paste</i><span class="menu-title" data-i18n="User">Roles</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('permissions.index')}}"><i class="material-icons">content_paste</i><span class="menu-title" data-i18n="User">Permissions</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('images.index')}}"><i class="material-icons">image</i><span class="menu-title" data-i18n="User">Images</span></a>
            </li> -->
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('students.exams')}}"><i class="material-icons">assignment</i><span class="menu-title" data-i18n="User">Exam List</span></a>
            </li>
            <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('students.result')}}"><i class="material-icons">rate_review</i><span class="menu-title" data-i18n="User">Exam Result</span></a>
            </li>
            <!-- <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="{{route('questions.index')}}"><i class="material-icons">help_outline</i><span class="menu-title" data-i18n="User">Questions</span></a>
            </li> -->
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
@endif
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
