@extends('layouts.master')
@section('content')
<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Image List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('images.index')}}">Images</a>
                            </li>
                            <li class="breadcrumb-item active">Image List
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <section class="users-list-wrapper section">
                    <div class="users-list-filter">
                    <!--     <div class="card-panel">
                            <div class="row">
                                <form action="{{route('permissions.index')}}">
                                    <div class="col m4">
                                        <label for="users-list-verified">Name</label>
                                        <div class="input-field">
                                            <input type="text" name="name_search" value="{{request('name_search')}}">
                                        </div>
                                    </div>
                                    <div class="col m4">
                                        <label for="users-list-role">Slug</label>
                                        <div class="input-field">
                                            <input type="text" name="slug_search" value="{{request('slug_search')}}">
                                        </div>
                                    </div>
                                    <div class="col s12 m6 l3 display-flex align-items-center right show-btn">
                                        <a class="btn btn-light mr-3" href="{{route('permissions.index')}}">Reset</a>
                                        <button type="submit" class="btn indigo waves-effect waves-light">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div> -->
                    </div>
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <a class="btn right mr-2 mb-2 indigo waves-effect waves-light" href="{{ route('images.create') }}">Add Image</a>
                                <!-- datatable start -->
                                @if (count($images)>0)  
                                <div class="responsive-table">
                                    <table id="users-list-datatable" class="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Image</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($images as $image)
                                                <tr>
                                
                                                    <td>{{$image->id}}</td>
                                                    <td><img src="{{asset('storage/'.$image->path)}}"
                                                            style="max-height: 100px;"></td> 
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        <div class="border-top">
                                            <h6 align="center" style="padding : 20px;">No Record Found.</h6>
                                        </div> 
                                    @endif
                                    
                                    
                                </div>
                               
                                <!-- datatable ends -->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- users list ends -->
                <!-- START RIGHT SIDEBAR NAV -->

        
                <!-- END RIGHT SIDEBAR NAV -->
                <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                    <ul>
                        <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                        <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                        <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                        <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                    </ul>
                </div>
            </div>
            <div class="content-overlay"></div>
        </div>
    </div>
</div>
@endsection
