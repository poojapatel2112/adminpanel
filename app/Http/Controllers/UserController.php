<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Alert;
use Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','id')->all();
        // echo '<pre>';print_r($roles);exit();
        return view('admin.users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
                    'user_email'=>'required|email|unique:users,email',
                    'user_name' => 'required',
                    'user_password' =>'required|same:user_confirm_password',
                    'user_role' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::to('users/create')
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $user = new User();
            $user->name = $request->user_name;
            $user->email = $request->user_email;
            $user->password = Hash::make($request->user_password);
            $user->role_id = $request->user_role;
            $user->save();
            // $user->assignRole($request->user_role);
            // echo '<pre>';print_r($user);exit();
            alert()->info('Role Added Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('roles.index'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        // echo '<pre>';print_r($user);exit();
        $user->delete();
        if($user){
            return response([
            'status'  => 'success',
            ]);
        }
        return redirect()->route('index');
    }

    public function importExportView()
    {
       return view('admin.users.import');
    }

    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function import(Request $request) 
    {
       $validator=Validator::make($request->all(),[
                    'file'=>'required|mimes:xlsx,xls'
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::to('users/importExportView')
            ->withErrors($validator)
            ->withInput();
        }
        ini_set('memory_limit', '-1');
        // echo '<pre>';print_r(request()->file('file'));exit();
        Excel::import(new UsersImport,request()->file('file'));
        // echo "success";
        // exit;
             
        return back();
    }
}
