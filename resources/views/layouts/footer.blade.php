	
	<footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
        <div class="footer-copyright">
            <div class="container"><span>&copy; 2020 <a href="http://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="https://pixinvent.com/">PIXINVENT</a></span></div>
        </div>
    </footer>

    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="{{asset('js/vendors.min.js')}}"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{asset('vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}">
    </script>
    <script src="{{asset('vendors/materialize-stepper/materialize-stepper.min.js')}}"></script>
    <script src="{{asset('vendors/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('vendors/jquery-validation/jquery.validate.min.js')}}"> </script>
    <script src="{{asset('vendors/chartist-js/chartist.min.js')}}"></script>
    <script src="{{asset('vendors/chartjs/chart.min.js')}}"></script>
    <script src="{{asset('fonts/fontawesome/js/all.min.js')}}"></script>
    <script src="{{asset('vendors/fullcalendar/lib/moment.min.js')}}"></script>
    <script src="{{asset('vendors/fullcalendar/js/fullcalendar.min.js')}}"></script>
    <script src="{{asset('vendors/fullcalendar/daygrid/daygrid.min.js')}}"></script>
    <script src="{{asset('vendors/fullcalendar/timegrid/timegrid.min.js')}}"></script>
    <script src="{{asset('vendors/fullcalendar/interaction/interaction.min.js')}}"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/search.js')}}"></script>
    <script src="{{asset('js/custom/custom-script.js')}}"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('js/scripts/cards-extended.js')}}"></script>
    <script src="{{asset('js/custom/custom-script.js')}}"></script>
    <script src="{{asset('js/scripts/dashboard-ecommerce.js')}}"></script>
    <script src="{{asset('js/scripts/page-users.js')}}"></script>
    <script src="{{asset('js/scripts/admin.js')}}"></script>
    <script src="{{asset('js/scripts/form-wizard.js')}}"></script>
     <script src="{{asset('js/scripts/css-animation.js')}}"></script>
     <script src="{{asset('js/scripts/advance-ui-modals.js')}}"></script>
     <script src="{{asset('js/scripts/app-calendar.js')}}"></script>
    <!-- END PAGE LEVEL JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
</body>

</html>