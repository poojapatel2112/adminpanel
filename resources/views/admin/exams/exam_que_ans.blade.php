@extends('layouts.master')
@section('content')

<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Question List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('exam_result.index')}}">Exam</a>
                            </li>
                            <li class="breadcrumb-item active">Question
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="row gradient-45deg-blue-indigo padding-1 medium-small">
                        <div class="col s12 m4 users-view-timeline">
                            <h6 class="indigo-text m-0 white-text">Total Mark: <span class="white-text">{{$student_exam->total_mark}}</span></h6>
                        </div>
                        <div class="col s12 m4 users-view-timeline">
                            <h6 class="indigo-text m-0 white-text">Obtain Mark : <span class="white-text">{{$student_exam->obtain_mark}}</span></h6>
                        </div>
                        <div class="col s12 m4 users-view-timeline">
                            @if($student_exam->status == 1)
                                <h6 class="indigo-text m-0 white-text">Status: <span class="white-text">PASS</span></h6>
                            @elseif($student_exam->status == 2)
                                <h6 class="indigo-text m-0 white-text">Status: <span class="white-text">fAIL</span></h6>
                            @else
                                <h6 class="indigo-text m-0 white-text">Status: <span class="white-text">PENDING</span></h6>
                            @endif
                        </div>
                    </div>
                    <ul class="stepper" id="nonLinearStepper">
                        @php $i=1; @endphp
                        @foreach($stu_que_ans as $question)
                        
                        <li class="step active" id="step_{{$i}}">
                                <div class="step-title waves-effect">Question {{$i}}
                                	<label class="right">
                                        <input class="validate_{{$question->id}} tnc_select1" id="tnc_select1{{$i}}" type="checkbox" value="1" {{ $question->status == '1' ? 'checked' : '' }} name="mark" data-url="{{route('exam_result.result')}}" data-attr="{{$question->id}}"/>
                                        <span></span>
                                    </label>
                                </div>
                                    <div class="step-content">
                                        <div class="row">
                                            <div class="input-field col m6 s12">
                                                <label for="firstName1">{{$question->questions->question}} <span class="red-text">*</span></label>
                                                <input type="hidden" value="{{$question->id}}" name="que_id" id="que_id_{{$i}}" class="que_id">
                                                <input type="hidden" value="{{$question->exam_id}}" name="exam_id" id="exam_id">
                                                <input type="hidden" value="{{$question->student_exams_id}}" name="stu_exam_id" id="stu_exam_id">
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col s12 mt-3">
                                             	@foreach($stu_questions as $que)
                                                @if($question->questions->question_type == 1)

                                                    @if($question->questions->id == $que->id)
                                                    	@foreach($que->answer as $ans)
	                                                        <label for="answer_{{$ans->id}}">
	                                                        
	                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}" type="radio" value="{{$ans->id}}" {{ $question->answer == $ans->id ? 'checked' : '' }} class="ans_id" data-attr="radio" required />
	                                                            <span>{{$ans->answer}}</span>
	                                                        </label>
                                                   		@endforeach
                                                   	@endif
                                                @endif
                                                @if($question->questions->question_type == 2)
                                                  
                                                @php $array_ans = explode(',',$question->answer); @endphp

                                                       @if($question->questions->id == $que->id)
                                                    	@foreach($que->answer as $ans)
	                                                        <label for="answer_{{$ans->id}}">
	                                                        
	                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}[]" type="checkbox" value="{{$ans->id}}" {{ in_array($ans->id, $array_ans) ? 'checked' : '' }} class="ans_id" data-attr="radio" required />
	                                                            <span>{{$ans->answer}}</span>
	                                                        </label>
                                                   		@endforeach
                                                   	@endif
                                                @endif
                                                @if($question->questions->question_type == 3)
                                                        @if($question->questions->id == $que->id)
                                                    	
	                                                        <label for="answer_{{$ans->id}}">
	                                                        
	                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}" type="text" value="{{$question->answer}}" class="ans_id" data-attr="radio" required />
	                                                        </label>
                                                   		
                                                   	@endif
                                                       
                                                @endif
                                                @if($question->questions->question_type == 4)
                                                       @if($question->questions->id == $que->id)
                                                    	
	                                                        <label for="answer_{{$ans->id}}">
	                                                        
	                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}" type="text" value="{{$question->answer}}" class="ans_id" data-attr="radio" required />
	                                                        </label>
                                                   		
                                                   	@endif
                                                       
                                                @endif
                                                @if($question->questions->question_type == 5)
                                                      @if($question->questions->id == $que->id)
                                                    
	                                                        <label for="answer_{{$ans->id}}">
	                                                        
	                                                            <input id="answer_{{$ans->id}}" name="answer_{{$i}}" type="text" value="{{$question->answer}}" class="ans_id" data-attr="radio" required />
	                                                           
	                                                        </label>
                                                   		
                                                   	@endif
                                                       
                                                @endif
                                                 @endforeach
                                            </div>
                                            
                                        </div>

                                    </div>
                        </li>
                        @php $i++; @endphp
                        @endforeach
                    </ul>
                    <form>
                        <input type="hidden" value="{{$question->student_exams_id}}" name="stu_exam_id" id="stu_exam_id">
                        <a class="btn-floating waves-effect waves-light green right  pass mt-3 mb-1" href="{{route('exam_result.final' ,['stu_exam_id' => $question->student_exams_id, 'status' => 1])}}">
                            <i class="material-icons ">check</i>
                        </a>
                        <a class="btn-floating waves-effect waves-light red right mr-2 mt-3 mb-1 fail" href="{{route('exam_result.final',['stu_exam_id' => $question->student_exams_id, 'status' => 2])}}">
                            <i class="material-icons ">clear</i>
                        </a>
                        <a class="btn-floating waves-effect waves-light btn-flat pink right mr-2 mt-3 mb-1" href="{{route('exam_result.pdf_generate' ,['stu_exam_id' => $question->student_exams_id])}}">
                            <i class="material-icons">cloud_download</i>
                        </a>
                        
                        
                    </form>
                </div>
                
            </div>
        </div>    
    </div>
</div>

   
@endsection