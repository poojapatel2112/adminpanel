<!DOCTYPE html>
<html>
<head>
	<title>Laravel 5 - Multiple markers in google map using gmaps.js</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="http://maps.google.com/maps/api/js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>


  	<style type="text/css">
    	#mymap {
      		border:1px solid red;
      		width: 980px;
      		height: 500px;
    	}
  	</style>


</head>
@extends('layouts.master')
@section('content')
<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Google Map</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('map')}}">Google MAp</a>
                            </li>
                            <li class="breadcrumb-item active">Google Search
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
  			<div id="mymap"></div>
  	</div>
</div>


<script type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=AIzaSyBXvkATHldPxhN0OAKyUkMbqZTx3WVreQ8 &libraries=places" ></script>
  <script type="text/javascript">


    var locations = <?php print_r(json_encode($locations)) ?>;


    var mymap = new GMaps({
      el: '#mymap',
      lat: 21.170240,
      lng: 72.831061,
      zoom:6
    });


    $.each( locations, function( index, value ){
	    mymap.addMarker({
	      lat: value.lat,
	      lng: value.long,
	      title: value.name,
	      click: function(e) {
	        alert('This is '+value.name);
	      }
	    });
   });


  </script>


@endsection