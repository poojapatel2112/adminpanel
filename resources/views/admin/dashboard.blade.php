@extends('layouts.master')

@section('content')
	<div id="main">
	<div class="row">
		<div class="col s12">
            <div class="container">
            	<h2 class="blue-text text-darken-2" id="welcome"><center>Welcome To Quiz Zone</center></h2>
	                <div class="col s12 m4 l4">
	                    <div id="chartjs" class="card pt-0 pb-0">
	                        <div class="padding-2 ml-2">
	                            <h5 class="mt-2 mb-0 font-weight-600">Total Exam</h5>
	                            <h5>{{$total_exam}}</h5>
	                            <i class="material-icons ">assessment</i>
	                        </div>
	                        <div class="sample-chart-wrapper card-gradient-chart">
	                            <canvas id="custom-line-chart-sample-one" class="center"></canvas>
	                        </div>
	                    </div>
	                </div>

	                <div class="col s12 m4 l4">
                        <div id="chartjs2" class="card pt-0 pb-0">
                            <div class="padding-2 ml-2">
                                <h5 class="mt-2 mb-0 font-weight-600">Total User</h5>
	                            <h5>{{$total_user}}</h5>
	                            <i class="material-icons ">people</i>
                            </div>
                            <div class="sample-chart-wrapper card-gradient-chart">
                                <canvas id="custom-line-chart-sample-two" class="center"></canvas>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4 l4">
                        <div id="chartjs3" class="card pt-0 pb-0">
                            <div class="padding-2 ml-2">
                                <h5 class="mt-2 mb-0 font-weight-600">Total User</h5>
	                            <h5>{{$total_user}}</h5>
	                            <i class="material-icons ">people</i>
                            </div>
                            <div class="sample-chart-wrapper card-gradient-chart">
                                <canvas id="custom-line-chart-sample-three" class="center"></canvas>
                            </div>
                        </div>
                    </div>
	           
			</div>
		</div>
	</div>
</div>

@endsection