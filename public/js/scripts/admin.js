$(document).ready(function () {

    $('#welcome').addClass('animated swing');
    $('#click_me').addClass('animated flip');

    $('.multiple').hide();
    var check = $('.question_type').val();
    var add = $('#addmore');
    var total = '';
    var input_field = $('#addField');
    var field = '<div class="input-field col s12 m3"><input id="question" name="answer[]" type="text" class="validate" value=""><label for="question">Answer</label><a class="remove_div waves-effect btn-flat right" id="remove_div"><i class="material-icons">delete</i></a></div>';
    if(check ==1 || check == 2)
    {
        $('.multiple').show();
    }
	$('.deleteUser').on('click', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-attr');
  	var url =  $(this).attr('data-url');
    swal({
	            title: 'Are you sure?',
	            type: 'warning',
	            text: 'This record will be permanantly deleted!',
	            icon: 'warning',
	            showCancelButton: true,

            },
    	function(value) {
                if(value){
                $.ajax({
                    type: "get",
                     url : 	url,
                    data: {id:id},
                    success: function (response) {
                        //alert(response);
                            if(response.status=="success"){
                                swal({
                                title: 'successs',
                                text: 'This record deleted successfully!!!',
                                type: 'success',
                                // button: [ "ok!"],
                                //location.reload();  //Refresh page
                                },
                                function(){
                                location.reload();
                                });
                            }
                        }         
                });
            }
            else {
                swal("You choose not to delete the record!");
            }
        });
    });

    $('#image').change(function(){
           
    let reader = new FileReader();
    reader.onload = (e) => { 
      $('#preview-image').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]); 
  
   });

    $('.question_type').change(function(){
        
        var type = $(this).val();
       
        if(type == 1 || type == 2)
        {
            $('.multiple').show();
        }
        else
        {
            $('.multiple').hide();
        }
        
   });

    
    $('#addmore').click(function(){
      
        // $(wrapper).append(fieldHTML); //Add field html

        $(input_field).append(field); 
    });

    $(input_field).on('click', '#remove_div', function(e){
        e.preventDefault();
        // $(this).closest('#question').remove(); //Remove field html
        $(this).parent('div').remove();
    });

    $('.next_btn').click(function(){
    
        var i = $(this).attr('data-attr');
        var q_id = "#que_id_"+i;
        var que_id = $(q_id).val();
        var total_que = $('#total_que').val();
        var exam_id = $('#exam_id').val();
        var stu_exam_id = $('#stu_exam_id').val();
        var a_id = "answer_"+i;
        var value =[];
        var ans_id = $("input[name="+a_id+"]").val();
        var redirect_url = "route('students.exams')";
        $('input[name="'+a_id+'"]:checked').map(function() {
            value.push($(this).val());
        });
        var url =  $(this).attr('data-url');
        var checkboxValues = [];
        $("input[name='answer']:checked").map(function() {
                checkboxValues.push($(this).val());
        });
       
        $.ajax({ 

            url: url , 
            method:"get",
            data: {
                que_id:que_id,
                value:value,
                ans_id:ans_id,
                exam_id:exam_id,
                stu_exam_id:stu_exam_id,
                total_que:total_que
            },
            success: function(response){ 
                if(response.status == 'success'){
                // alert(response.total_que);
                    i++;
                    var id = "#step_"+i;
                    var j = i-1;
                    var pre_id = "#step_"+j;

                    $('#stu_exam_id').val(response.id); 
                    $('#total_que').val(response.total_que);
                    $(id).addClass( "active" );
                    $(pre_id).removeClass( "active" );
                }
                if(response.status == 'completed'){
                    swal({
                        title: 'success',
                        text: 'You completed quiz!!!',
                        type: 'success',
                        // button: [ "ok!"],
                        //location.reload();  //Refresh page
                        },
                        
                    );
                    window.location = response.redirect_url;
                   
                }
                else{
                  $('.errorTxt1').html(response.error.ans_id);
                }
            } 
        }); 
        
    });

    $('.pre_btn').click(function(){
        var i = $(this).attr('data-attr');
        var id = "#step_"+i;
        var j = i-1;
        var pre_id = "#step_"+j;
        if(j == 0)
        {
            $(id).addClass( "active" );
        }
        else
        {    
            $(pre_id).addClass( "active" );
            $(id).removeClass( "active" );
        }

    });

    $('.tnc_select1').click(function(){
        // var i = $(this).val();

        var exam_id = $('#stu_exam_id').val();
        var que_id = $(this).attr('data-attr');
        var check = $('input[name=mark]:checked').length;
        if($(this).prop("checked") == true){
               var i = 1; 
        }
        else
        {
            var i = 0;
        }
        var url =  $(this).attr('data-url');
       
        $.ajax({ 

            url: url , 
            method:"get",
            data: {
                total:check,
                stu_que_ans_id:que_id,
                value:i,
                stu_exam_id:exam_id
            },
            success: function(response){ 
                // 
            } 
        }); 
        //
    });

    // $('#click_me').click(function(){
    //     alert("hiii")
    //      $('.bd-example-modal-lg').modal('show');
    // });


});