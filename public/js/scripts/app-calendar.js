/* Calendar */
/*-------- */

$(document).ready(function () {

    $.ajaxSetup({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  
  /* initialize the calendar
   -----------------------------------------------------------------*/
  var Calendar = FullCalendar.Calendar;
  var Draggable = FullCalendarInteraction.Draggable;
  var containerEl = document.getElementById('external-events');
  var calendarEl = document.getElementById('fc-external-drag');
  var checkbox = document.getElementById('drop-remove');

  //  Basic Calendar Initialize
  var basicCal = document.getElementById('basic-calendar');
  
  var fcCalendar = new FullCalendar.Calendar(basicCal, {
    defaultDate: new Date(),
    editable: true,
    plugins: ["dayGrid", "interaction"],
    eventLimit: true, // allow "more" link when too many events
    events: getsiteurl()+ "/calender",
    eventColor: '#378006',
    displayEventTime: false,
    themeSystem: 'bootstrap5',
    selectable: true,
                    eventRender: function (event, element, view) {

                        if (event.allDay === 'true') {
                                event.allDay = true;
                                eventColor: '#378006';
                        } else {
                                event.allDay = false;
                                eventColor: '#378006';
                        }
                    },
                    
                    selectHelper: true,

                    select: function (info, allDay) {
                        var title = prompt('Event Title:');
                        if (title) {
                            var start = info.startStr;
                            var end = info.endStr;
                           
                            $.ajax({
                                url: getsiteurl()+ "/calenderAjax",
                                data: {
                                    title: title,
                                    start: start,
                                    end: end,
                                    type: 'add'
                                },
                                type: "POST",
                                success: function (data) {
                                    displayMessage("Event created Successfully");
  
                                    fcCalendar.FullCalendar.Calendar('renderEvent',
                                        {
                                            id: data.id,
                                            title: title,
                                            start: start,
                                            end: end,
                                            allDay: allDay
                                        },true);
  
                                    fcCalendar.FullCalendar.Calendar('unselect');
                                }
                            });
                        }
                    },
                    eventDrop: function (event, delta) {
                        var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
                        var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD");
  
                        $.ajax({
                            url: SITEURL + '/fullcalenderAjax',
                            data: {
                                title: event.title,
                                start: start,
                                end: end,
                                id: event.id,
                                type: 'update'
                            },
                            type: "POST",
                            success: function (response) {
                                displayMessage("Event Updated Successfully");
                            }
                        });
                    },
                    eventClick: function (event) {
                        var deleteMsg = confirm("Do you really want to delete?");
                        if (deleteMsg) {
                            $.ajax({
                                type: "POST",
                                url: SITEURL + '/fullcalenderAjax',
                                data: {
                                        id: event.id,
                                        type: 'delete'
                                },
                                success: function (response) {
                                    fcCalendar.FullCalendar.Calendar('removeEvents', event.id);
                                    displayMessage("Event Deleted Successfully");
                                }
                            });
                        }
                    }
  });
  fcCalendar.render();

  // initialize the calendar
  // -----------------------------------------------------------------
  // var calendar = new Calendar(calendarEl, {
  //   header: {
  //     left: 'prev,next today',
  //     center: 'title',
  //     right: "dayGridMonth,timeGridWeek,timeGridDay"
  //   },
  //   editable: true,
  //   plugins: ["dayGrid", "timeGrid", "interaction"],
  //   droppable: true, // this allows things to be dropped onto the calendar
  //   defaultDate: '2019-01-01',
  //   events: [
  //     {
  //       title: 'All Day Event',
  //       start: '2019-01-01',
  //       color: '#009688'
  //     },
  //     {
  //       title: 'Long Event',
  //       start: '2019-01-07',
  //       end: '2019-01-10',
  //       color: '#4CAF50'
  //     },
  //     {
  //       id: 999,
  //       title: 'Meeting',
  //       start: '2019-01-09T16:00:00',
  //       color: '#00bcd4'
  //     },
  //     {
  //       id: 999,
  //       title: 'Happy Hour',
  //       start: '2019-01-16T16:00:00',
  //       color: '#3f51b5'
  //     },
  //     {
  //       title: 'Conference Meeting',
  //       start: '2019-01-11',
  //       end: '2019-01-13',
  //       color: '#e51c23'
  //     },
  //     {
  //       title: 'Meeting',
  //       start: '2019-01-12T10:30:00',
  //       end: '2019-01-12T12:30:00',
  //       color: '#00bcd4'
  //     },
  //     {
  //       title: 'Dinner',
  //       start: '2019-01-12T20:00:00',
  //       color: '#4a148c'
  //     },
  //     {
  //       title: 'Birthday Party',
  //       start: '2019-01-13T07:00:00',
  //       color: '#ff5722'
  //     },
  //     {
  //       title: 'Click for Google',
  //       url: 'http://google.com/',
  //       start: '2019-01-28',
  //     }
  //   ],
  //   drop: function (info) {
  //     // is the "remove after drop" checkbox checked?
  //     if (checkbox.checked) {
  //       // if so, remove the element from the "Draggable Events" list
  //       info.draggedEl.parentNode.removeChild(info.draggedEl);
  //     }
  //   }
  // });
  // calendar.render();

  // initialize the external events
  // ----------------------------------------------------------------

  //   var colorData;
  // $('#external-events .fc-event').each(function () {
  //   // Different colors for events
  //   $(this).css({ 'backgroundColor': $(this).data('color'), 'borderColor': $(this).data('color') });
  // });
  // var colorData;
  // $('#external-events .fc-event').mousemove(function () {
  //   colorData = $(this).data('color');
  // })
  // Draggable event init
  new Draggable(containerEl, {
    itemSelector: '.fc-event',
    eventData: function (eventEl) {
      return {
        title: eventEl.innerText,
       
      };
    }
  });
});

function displayMessage(message) {
    toastr.success(message, 'Event');
} 