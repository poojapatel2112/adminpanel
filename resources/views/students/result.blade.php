@extends('layouts.master')
@section('content')

<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Exam List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a>Exam</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <section class="users-list-wrapper section">
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <!-- datatable start -->
                                @if (count($exam_list)>0)  
                                <div class="responsive-table">
                                    <table id="users-list-datatable" class="table">
                                        <thead>
                                            <tr>
                                               
                                                <th>No</th>
                                                <th>Exam Name</th>
                                                <th>Total Mark</th>
                                                 <th>Obtain Mark</th>
                                                <th>Status</th>
                                                <th>Download</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                                @php $i=1; @endphp
                                                @foreach($exam_list as $exam)
                                                    <tr>
                                
                                                        <td>{{$i}}</td>
                                                        <td>{{$exam->exam_name->name}}</td> 
                                                        <td>{{$exam->total_mark}}</td> 
                                                        <td>{{$exam->obtain_mark}}</td> 
                                                        @if ($exam->status == 1)
                                                        <td>Pass</td>
                                                        @elseif($exam->status == 2)
                                                        <td>Fail</td>
                                                        @else
                                                        <td>Pending</td>
                                                        @endif
                                                        <td><a href="{{route('exam_result.pdf_generate' ,['stu_exam_id' => $exam->id])}}" data-position="bottom" data-tooltip="Result PDF" class="tooltipped"><i class="material-icons ">file_download</i></a></td>
                                                    </tr>
                                                    @php $i++; @endphp
                                                @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        <div class="border-top">
                                            <h6 align="center" style="padding : 20px;">No Record Found.</h6>
                                        </div> 
                                    @endif
                                  
                                    
                                </div>
                               
                                <!-- datatable ends -->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- users list ends -->
                <!-- START RIGHT SIDEBAR NAV -->

        
                <!-- END RIGHT SIDEBAR NAV -->
                <!-- <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                    <ul>
                        <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                        <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                        <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                        <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                    </ul>
                </div> -->
            </div>
            <div class="content-overlay"></div>
        </div>
    </div>
</div>

@endsection