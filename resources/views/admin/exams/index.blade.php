@extends('layouts.master')
@section('content')

<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Role List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="{{route('roles.index')}}">{{ __('sentence.role')}}</a>
                            </li>
                            <li class="breadcrumb-item active">{{ __('sentence.role_list')}}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <section class="users-list-wrapper section">
                    
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <a class="btn right mr-2 mb-2 indigo waves-effect waves-light" href="{{ route('exams.create') }}">Add Exam Type</a>
                                <!-- datatable start -->
                                @if (count($exams)>0)  
                                <div class="responsive-table">
                                    <table id="users-list-datatable" class="table">
                                        <thead>
                                            <tr>
                                               
                                                <th>No</th>
                                                <th>Exam Type</th>
                                                <th>Questions</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                                @foreach($exams as $exam)
                                                    <tr>
                                
                                                        <td>{{$exam->id}}</td>
                                                        <td>{{$exam->name}}</td> 
                                                        <td><a href="{{route('exams.question',$exam->id)}}"><i class="material-icons">playlist_add</i>Question</a></td>
                                                        <td>    
                                                            <a href="{{route('exams.edit',$exam->id)}}"><i class="material-icons">edit</i></a>
                                                            <a href="" data-toggle="tooltip" data-placement="top"  data-original-title="Delete"  class="deleteUser" data-id="{{$exam->id}}" data-attr="{{$exam->id}}" data-url ="{{route('exams.delete',$exam->id)}}" ><i class="material-icons">delete</i></a>
                                                        </td>
                                                        <!-- <a href="#"><i class="material-icons">delete</i></a></td> -->
                                                    </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                        <div class="border-top">
                                            <h6 align="center" style="padding : 20px;">No Record Found.</h6>
                                        </div> 
                                    @endif
                                  
                                    
                                </div>
                               
                                <!-- datatable ends -->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- users list ends -->
                <!-- START RIGHT SIDEBAR NAV -->

        
                <!-- END RIGHT SIDEBAR NAV -->
                <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                    <ul>
                        <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                        <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                        <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                        <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                    </ul>
                </div>
            </div>
            <div class="content-overlay"></div>
        </div>
    </div>
</div>

@endsection