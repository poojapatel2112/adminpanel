<?php

namespace App\Http\Controllers;

use App\Models\Student_exam;
use App\Models\Student_question_answer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Question;
use PDF;
use Mail;

class StudentExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student_exam = Student_exam::with('exam_name')->get();
        // echo '<pre>';print_r($student_exam);exit;
        return view('admin.exams.exam_result',compact('student_exam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student_exam  $student_exam
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stu_que_ans = Student_question_answer::with('questions','answer')->where('student_exams_id',$id)->get();
        $student_exam = Student_exam::find($id);
        $stu_questions = Question::with('answer')->where('exam_id',$student_exam->exam_id)->get();
        // $que_ans = Question::with('answer')->where('')get();
        // $que_ans = DB::table('student_question_answers')->select('answer','id')->where('student_question_answers.student_exams_id',$id)
        //         ->get();
        // foreach($que_ans as $ans)
        // {
        //     $array[$ans->id] = explode(",", $ans->answer );
        
        // $stu_que =DB::table('student_question_answers')
        //         ->leftjoin('questions', 'student_question_answers.question_id', '=', 'questions.id')
        //         ->leftjoin('answers', 'student_question_answers.answer', '=','answers.id')
        //         ->select('student_question_answers.*','questions.question as question','questions.question_type as q_type', 'answers.answer as answer1')
        //         ->where('student_question_answers.student_exams_id',$id)
        //         ->get();
        // }
        // echo '<pre>';print_r($stu_que_ans);exit;
        return view('admin.exams.exam_que_ans',compact('stu_que_ans','stu_questions','student_exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student_exam  $student_exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Student_exam $student_exam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student_exam  $student_exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student_exam $student_exam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student_exam  $student_exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student_exam $student_exam)
    {
        //
    }

    public function result(Request $request)
    {

        $student_exam = Student_exam::find($request->stu_exam_id);
        $stu_que_ans = Student_question_answer::find($request->stu_que_ans_id);
        $student_exam->obtain_mark = $request->total;
        $student_exam->save();
        $stu_que_ans->status = $request->value;
        $stu_que_ans->save();
        return response()->json(['status'=>'success','id'=> $stu_que_ans->student_exams_id]);
        

    }

    public function final_result(Request $request)
    {
        // print_r($request->all());exit;
        $student_exam = Student_exam::find($request->stu_exam_id);
        $student_exam->status = $request->status;
        $student_exam->save();
        $student = User::where('id', $student_exam->student_id)->first();
        $student_email = $student->email;
        $data["email"] = $student_email;
        $data["title"] = "Exam results";
        $data["body"] = "This is your exam result.";
        $stu_que_ans = Student_question_answer::with('questions','ans')->where('student_exams_id',$request->stu_exam_id)->get();
        $student_exam = Student_exam::find($request->stu_exam_id);
        $stu_questions = Question::with('answer')->where('exam_id',$student_exam->exam_id)->get();
        // echo '<pre>';print_r($stu_questions);exit;
        $pdf = PDF::loadView('admin.exams.myPDF', compact('stu_que_ans','student_exam','stu_questions'));
        Mail::send('admin.exams.myemail', $data, function($message)use($data,$pdf) {
            $message->to($data["email"], $data["title"])
            ->subject($data["body"])
            ->attachData($pdf->output(), "result.pdf");
            });
        alert()->info('Status Updated Successfully.')->persistent('close')->autoclose("3600");
        return Redirect()->route('exam_result.index'); 

    }

    public function generatePDF(Request $request)
    {
        // print_r($request->all());
        $stu_que_ans = Student_question_answer::with('questions','ans')->where('student_exams_id',$request->stu_exam_id)->get();
        $student_exam = Student_exam::find($request->stu_exam_id);
        $stu_questions = Question::with('answer')->where('exam_id',$student_exam->exam_id)->get();
        // echo '<pre>';print_r($stu_questions);exit;
        $pdf = PDF::loadView('admin.exams.myPDF', compact('stu_que_ans','student_exam','stu_questions'));
        return $pdf->download('ExamResult.pdf');
    }
}
