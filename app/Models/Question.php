<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = [
        'exam_id',
        'question',
        'is_mandatory',
        'question_type',
    ];

    public function exams()
    {
        return $this->belongsTo(Exam::class);
    }

    public function answer()
    {
        return $this->hasMany(Answer::class,'question_id');
    } 
}
