@extends('layouts.master')
@section('content')

<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Exam List</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="index.html">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Exam
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
         <div class="col s12">
            <div class="container">
                @foreach($exams as $exam)
                <div class="col s12 m3">
                    <div class="card gradient-shadow gradient-45deg-blue-indigo border-radius-3">
                        <div class="card-content center border-radius-6 mt-10 card-animation-1">
                            <img src="{{asset('images/gallery/intro-slide-1.png')}}" alt="images" class="width-40 circle responsive-img border-radius-8 z-depth-4 image-n-margin" />               
                            <h5></h5>
                            <h5 class="white-text lighten-4"><a href="{{route('students.questions',$exam->id)}}" class="white-text collection-item">{{$exam->name}}</a></h5>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

    </div>
</div>


@endsection