@extends('layouts.master')
@section('content')
<div id="main">
        <div class="row">
            <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
                <!-- Search for small screen-->
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <h5 class="breadcrumbs-title mt-0 mb-0"><span>User Add</span></h5>
                        </div>
                        <div class="col s12 m6 l6 right-align-md">
                            <ol class="breadcrumbs mb-0">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">User</a>
                                </li>
                                <li class="breadcrumb-item active">User Add
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="container">
                    <!-- users edit start -->
                    <div class="section users-edit">
                        <div class="card">
                            <div class="card-content">
                                <!-- <div class="card-body"> -->
                                
                                <div class="row">
                                    <div class="col s12" id="account">
                                        <!-- users edit media object start -->
                                        <!-- users edit account form start -->
                                        <form id="accountForm" method="post" action="{{route('users.store')}}">
                                        	{{ csrf_field() }}
                                            <div class="row">
                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="user_name" name="user_name" type="text" class="validate" value="{{ old('user_name') }}">
                                                            <label for="rolename">User Name</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('user_name') }}</div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="user_email" name="user_email" type="text" class="validate" value="{{ old('user_email') }}" data-error=".errorTxt2">
                                                            <label for="roleslug">User Email</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('user_email') }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="user_password" name="user_password" type="text" class="validate" value="{{ old('user_password') }}" data-error=".errorTxt2">
                                                            <label for="roleslug">User Password</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('user_password') }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <input id="user_confirm_password" name="user_confirm_password" type="password" class="validate" value="{{ old('user_confirm_password') }}" data-error=".errorTxt2">
                                                            <label for="roleslug">User Confirm Password</label>
                                                            <div id="username-error" class="errorTxt1 error invalid-feedback">{{ $errors->first('user_confirm_password') }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12 m6">
                                                    <div class="row">
                                                        <div class="col s12 input-field">
                                                            <select class="form-control" id="users-list-verified" name=user_role>
                                                               
                                                                @foreach($roles as $key => $value)
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                                
                                                            </select>
                                                            <label for="users-list-verified">User Role</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col s12">
                                                    <table class="mt-1">
                                                        <thead>
                                                            <tr>
                                                                <th>Module Permission</th>
                                                                <th>Read</th>
                                                                <th>Write</th>
                                                                <th>Create</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Users</td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Articles</td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Staff</td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" checked />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <label>
                                                                        <input type="checkbox" />
                                                                        <span></span>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!-- </div> -->
                                                </div>
                                                <div class="col s12 display-flex justify-content-end mt-3">
                                                    <button type="submit" class="btn indigo">
                                                        Save role</button>
                                                    <a href="{{route('roles.index')}}" class="btn btn-light"  >Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- users edit ends -->
                    <!-- START RIGHT SIDEBAR NAV -->
                    
                    <!-- END RIGHT SIDEBAR NAV -->
                    <div style="bottom: 50px; right: 19px;" class="fixed-action-btn direction-top"><a class="btn-floating btn-large gradient-45deg-light-blue-cyan gradient-shadow"><i class="material-icons">add</i></a>
                        <ul>
                            <li><a href="css-helpers.html" class="btn-floating blue"><i class="material-icons">help_outline</i></a></li>
                            <li><a href="cards-extended.html" class="btn-floating green"><i class="material-icons">widgets</i></a></li>
                            <li><a href="app-calendar.html" class="btn-floating amber"><i class="material-icons">today</i></a></li>
                            <li><a href="app-email.html" class="btn-floating red"><i class="material-icons">mail_outline</i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>
@endsection