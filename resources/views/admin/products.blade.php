@extends('layouts.master')

@section('content')
<div id="main">
    <div class="row">
        <div class="pt-3 pb-1" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Counter</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Counter
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
           
        <div class="col s12">
            <div class="container">
               	<section class="users-list-wrapper section">
                    <div class="users-list-filter">
                        <div class="card-panel">
                            <div class="row">
                                <form method="GET" action="{{ route('products') }}">

				                    <div class="row mb-5">
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <input type="text" name="product_search" class="form-control"
				                                    placeholder="Search by name" value="{{ old('product_search') }}">
				                            </div>
				                        </div>
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <button class="btn btn-success">Search</button>
				                            </div>
				                        </div>
				                    </div>
				                </form>
                            </div>
                        </div>
                    </div>
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <!-- datatable start -->
                                @if (count($products)>0)  
                                <div class="responsive-table">
                                    <table id="users-list" class="table users-list-datatable">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Price</th>
                                            
                                                <th>Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($products as $product)
                                                <tr>
                                                
                                                    <td>{{$product->id}}</td>
                                                    <td>{{$product->name}}</td> 
                                                    <td>{{$product->price}}</td>
                                                   
                                                    <td>{{$product->product_type}}</td>
                                                    <td><a href="{{route('product.replicate',$product->id)}}" data-toggle="tooltip" data-placement="top"  data-original-title="Replicate"  class="replicatePro" data-id="{{$product->id}}" data-attr="{{$product->id}}" ><i class="material-icons">cached</i></a></td>
                                                </tr>
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                                @endif
                                <!-- datatable ends -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
	</div>
</div>
@endsection