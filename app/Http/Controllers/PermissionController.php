<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Alert;


class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Permission::select();
        if(isset($request->name_search) && !empty($request->name_search))
        {
            $data->where('name','like', '%'.$request->name_search.'%');
        }
        if(isset($request->slug_search) && !empty($request->slug_search))
        {
            $data->where('guard_name','like', '%'.$request->slug_search.'%');
        }
        $permissions = $data->get();
        return view('admin.permissions.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
                    'permission_slug'=>'required',
                    'permission_name' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect::to('permissions/create')
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $permission = new permission();
            $permission->name = $request->permission_name;
            $permission->guard_name = $request->permission_slug;
            $permission->save();
            
            alert()->info('Permission Added Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('permissions.index'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::find($id);
        return view('admin.permissions.edit',compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
                    'permission_slug'=>'required',
                    'permission_name' => 'required',
        ]); 
        if ($validator->fails()) {
            // Alert::error('Oops, Invalid file type or file seems to be missing!');
            // return back();
            return Redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
            $permission = Permission::find($id);
            $permission->name = $request->permission_name;
            $permission->guard_name = $request->permission_slug;
            $permission->save();
            alert()->info('Permission Updated Successfully.')->persistent('close')->autoclose("3600");
            return Redirect()->route('permissions.index'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);
        $permission->delete();
        if($permission){
            return response([
            'status'  => 'success',
            ]);
        }
        return redirect()->route('permissions.index');
    }
}
