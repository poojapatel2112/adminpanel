<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student_question_answer extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_exams_id',
        'question_id',
        'answer',
        'status',
    ];

    public function questions()
    {
        return $this->belongsTo(Question::class,'question_id');
    }

    public function answer()
    {
        return $this->belongsTo(Answer::class,'answer');
    }

    public function ans()
    {
        return $this->belongsTo(Answer::class,'answer');
    }


    // public function answer()
    // {
    //     return $this->hasMany(Answer::class,'answer');
    // } 

}
